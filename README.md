# certifications

There are alot more of certifications but this ones are the ones that I am more interested in doing. 

> Always choose the best path for you 

## Azure 

- [ ] Fundamentals
  - [x] AZ-900
  - [ ] SC-900
  - [ ] Optionals
    - [ ] DP-900
    - [ ] AI-900
- [ ] Associate 
  - [ ] AZ-500
  - [ ] SC-200
  - [ ] Optionals
    - [ ] DP-203
    - [ ] AZ-204
    - [ ] SC-400
- [ ] Expert
  - [ ] AZ-400
  - [ ] Optional
    - [ ] AZ-303
    - [ ] AZ-304

## AWS

- [ ] Foundational 
  - [ ] Cloud Practitioner
- [ ] Associate 
  - [ ] Solution Architect 
  - [ ] SysOps administrator
- [ ] Professional
  - [ ] DevOps Engineer
  - [ ] Solutions Architect 
- [ ] Specialty
  - [ ] Security 
  - [ ] Advanced Networking 
  - [ ] Optional
    - [ ] Data Analytics
