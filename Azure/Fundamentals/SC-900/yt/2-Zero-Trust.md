# Zero Trust Methodologies 

Best way to protect data 

> Everything is open and untrust network

> Trust no one, verify everything 

## Zero Trust Guiding Principles 

1. Verify explicitly 
2. Least privileged access 
3. Assume breach

### Verify explicitly 


Always authnticate and authorize based on the available data points including (user identity, location, device, security or workload data classification)

### Least Privileged access 

This means that you should limit user access with just in time and just enough access which is usually know as jit or jea. Other ways you can limit the privileged access is by implementing risk-based adaptive policies and data protection to protect both , data and productivity. 

### Assuam breach all the time 

Segment access by network, user devices and applications and you should use encryption to protect data and use analytics to gain visibility and detec threats and improve your security posture. 

## Six Foundational Pillars 

* Identities 
* Devices/Endpoints
* Data 
* App 
* Infrastructure 
* Network 

### Identities 

* Users 
* Services 
* Devices 

[img](./img/Identities.png)

### Devices 

Large attack surface as data flows from devices to on-premises workloads and the cloud. 
Monitoring devices for health and compliance 

### Data 

Should be classified, labeled and encrypted based on its attributes. Security efforts are ultimately about protecting data and ensuring it reamins safe when it leaves devices applications, infrastructure and networks that the organization controls.

### App

Are the way that data is consumed, this includes discovering all applications being used sometimes called shadow i.t because not all applications are managed centrally. 
This include mannaging permissions and access 

### Infrastructure 

To impove security you access for version configuration and just in time access and use telemetry to detect attacks and the anomalies. This allows you to automatically block or flag risky behavior and take proactive actions 

### Network

Should be segmented includeing deeper in-network micro segmentation also real-time threat protection end-to-end encryption, monitoring and analytics.

