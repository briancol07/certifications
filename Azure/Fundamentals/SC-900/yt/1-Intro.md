# Intro 

## Who is this course for?

Fundamentals of security 
Students 
IT professionals 
ETC 

## Study Areas 

Area | Percent 
-----|--------
Describe the concept of security, compliance and identity |10-15%
Describe the capabilities of Microsoft Identity and access management solution | 30-35% 
Describe the capabilities of Microsoft security solutions |35-40%
Describe the capabilities of Microsoft compliance solution | 25-30%


