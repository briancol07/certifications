# Shared Responsibility Model 

The shared responsibility model identifies which security task are handled by the cloud provider, and which security taks are handled by you.

* Software as a service (SaaS)
* Platform as a Service (PaaS)
* Infrastructure as a Service (IaaS)
* On-Premices datacenter 

[SRM](./img/shared-responsibility-model.png)

* On-Premises datacenter 
  * You have responsibility for everything from physical security to encryption sensitive data 
* IaaS 
  * Not resposible for physical component 
    * Computers 
    * Network 
    * Physical security of the datacenter 
  * Responsibility for software components 
    * OS 
    * Network controls 
    * Applications 
    * Protecting data 
* PaaS
  * The cloud provides manages 
    * Hardware 
    * OS 
  * Customer
    * Applications
    * Data
* SaaS
  * The cloud provider 
    * Everything 
  * Customer
    * Data
      * Devices 
      * Accounts 
      * Identities 

## Responsibilities always retained 

* Information and data 
* Device (mobile and PCs)
* Account and Identities 


