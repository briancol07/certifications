# Defense in Depth 

> Uses a leyered approach to security 

A defense in-Depth strategy uses a series of mechanisms to slow the advance of an attack. Each layer provides protection so that, if one layer is breached, a subsequent layer will prevent an attacker getting unauthorized access to data.


┌───────────────────────────────┐
│       Physical Security       │ 
│ ┌───────────────────────────┐ │
│ │     Identity & Access     │ │
│ │ ┌──────────────────────┐  │ │
│ │ │        Perimeter     │  │ │
│ │ │ ┌───────────────────┐│  │ │
│ │ │ │      Network      ││  │ │
│ │ │ │  ┌──────────────┐ ││  │ │
│ │ │ │  │   Compute    │ ││  │ │
│ │ │ │  │ ┌───────────┐│ ││  │ │
│ │ │ │  │ │Application││ ││  │ │
│ │ │ │  │ │ ┌──────┐  ││ ││  │ │
│ │ │ │  │ │ │ Data │  ││ ││  │ │
│ │ │ │  │ │ └──────┘  ││ ││  │ │
│ │ │ │  │ └───────────┘│ ││  │ │
│ │ │ │  └──────────────┘ ││  │ │
│ │ │ └───────────────────┘│  │ │
│ │ └──────────────────────┘  │ │
│ └───────────────────────────┘ │
└───────────────────────────────┘

### Physical 

Security such as limiting access to a datacenter to only authorized personnel

### Indentity and access

Security controls, such as multifactor authentication or conditon-based access, to control access to infrastructure and change control 

### Perimeter 

Security of your corporate network includes distributed denial of service (DDoS) protection to filter large-scale attacks before they can cuase a denial of service for users 

### Network 

Security, such as network segmentation and network access controls, to limit communicaiton between resources 

### Compute 

Layer security such as securing access to virtual machines either on-premises or in the cloud by closing certain ports 

### Aplication 

Layer security to ensure application are secure and free of security vulnerabilities 

### Data Layer

Security including controls to manage access to business and customer data and encryption to protect data 

## Confidentiality, Integrity, Availability (CIA) 

The triad 

### Confidentiality 

Refers to the need to keep confidential sensitive data such as customer information, password or financial data. You can encrypt data to keep it confidential, but then you also need to keep the encryption keys confidential. 

### Integrity 

Refers to keeping data or messages correct. When you send an email message, you want to be sure that the message received is the same as the message you sent.
When you store data in a database you want to be sure that the data you retrieve is the same as the data you stored.
Encrypting data keep it confidential but you must then be able to decrypt it so htat it's the same as before it was encrypted.

> Is about having confidence that the data hasn't been tampered with or altered 

### Availability 

Refers to making data availabe to those who need it, when they  need it. It's important to hte organization to keep customer data secure. but at the same time it must also be available to employees whoe deal with customers. While it might be more secure to store the data in an encrypted format, employees need access to decrypted data.
