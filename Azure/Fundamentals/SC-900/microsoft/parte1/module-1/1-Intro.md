# Introduccion 

> Las organizaciones deben comprender como proteger mejor sus datos 

## Despues de este modulo 

* Describe the shared responsibility and the defense in-depth security models 
* Describe the Zero-Trust model 
* Describe the concepts of encryption and hashing 
* Describe some basic compliance concepts 


