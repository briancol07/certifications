# Discuss Azure fundamental concepts 

## Overview 

* [2-Discuss-different-types-of-cloud-models.md](2-Discuss-different-types-of-cloud-models.md)
* [3-Describe-cloud-benefits-and-considerations.md](3-Describe-cloud-benefits-and-considerations.md)
* [4-Describe-different-Cloud-Services.md](4-Describe-different-Cloud-Services.md)
