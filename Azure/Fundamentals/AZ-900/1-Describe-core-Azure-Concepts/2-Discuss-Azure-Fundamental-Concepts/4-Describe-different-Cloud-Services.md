# Describe different cloud services 

## What are cloud service models? 

* PaaS
* IaaS
* SaaS

### Infrastructure as a Service (Iaas)

This cloud service model is the closest to managing physical servers; a cloud provider will keep the HW up-to-date, but operating system maintenance and network configuration is up to you as the cloud tenant. 
For example , Azure virtual machines are fully operational virtual compute devices running in Microsoft datacenters. An advantage of the cloud service model is rapid deployment of new compute devices. Stetting up a new VM is considerable faster than procuring, installing and configuring a physical server. 

Is the most flexible category of cloud services. It aims to give you complete control over the HW that runs your application. Instead of buying HW, with IaaS you rent it.

#### Advantages 

* No CapEx:
  * Users have no up-front costs.
* Agility : 
  * Application can be made accessible quickly, and deprovisioned whenever needed.
* Management:
  * The shared responsability model applies; The user manages and maintains the services they have provisioned, and the cloud provider manages and maintains the cloud infrastructure,
* Consumption-based model:
  * Organizations pay only for what they use and operate under an Operational Expenditure (OpEx) model
* Skill:
  * No deep technical skills are required to deploy, use and gain the benefits of a public cloud. Organizations can use the skills and expertise of the cloud provider to ensure workloads are secure, safe and highly available .
* Cloud Benfits: 
  * Organizations can use the skills and expertise of the cloud provider to ensure workloads are made secure and highly available. 
* Flexibility : 
  * Iaas is the most flexible cloud service because you have control to configure and manage the HW running your application.

### Platform as a Service (PaaS) 

This cloud service model is a managed hosting environment. The cloud provider manages thevirtual machine and networking resources, and the cloud tenant deploys theri applications into the managed hosting environment. For example, Azure App service Provides a managed hosting environment wheere developers can upload their web applications, without habing to worry about the physical HW and SW requirements. 

> PaaS provides the same benefits and considerations as IaaS, but there are some additional benefits to be aware of 

#### Advantages 

* No CapEx
  * Users have no up-front costs 
* Agility 
  * PaaS is more agile than Iaas, and users don't need to configure servers for running applications 
* Consumption-based model 
  * Users pay only for what the use, and opeerate under an OpEx model. 
* Skills 
  * No deep technical skills are required to deploy, use, and gian the benefits of PaaS
* Cloud benefits 
  * Users can take advantage of the skills and expertise of the cloud provider to ensure that their workloads are made secure and hightly avialable. In addition, users can gain acces to more cutting-edge development tools. They can then apply these tools across an application's lifecycle. 
* Productivity 
  * Users can focus on application development only , because the cloud provider handles all platform management. Working with distributed teams as services is easier because the platform is accessed over the internet. You can make the platform available globally more easily 

#### Disadvantage

Platform limitations : There can be some limitations to a cloud platform that might affect how an application runs. When you're evaluating which PaaS platform is best suited for a workload, be usre to consider any limitations in this area.

### Software as a Service (SaaS)

In this cloud service model, the cloud provider manages all aspects of the application environment such as virtual machines, networking resources, data storage and applications. The cloud tenant only needs to provide their data to the application managed by the cloud provider. For example, Microsoft office 365 provides a fully working version of Microsoft office that runs in the cloud. All you need to do is create your content and Office 365 take care of everything else.

SaaS is SW that's centrally hosted and managed for you and your users or curtomers. Usually one version of the application is used for all customers, and it's licensed through a monthly or annual subscription.

#### Advantages 

* No CapEx
  * Users have no up-front costs
* Agility 
  * Users can provide staff with access to the latest SW quickly and easily 
* Pay-as-you-go pricing model
  * Users pay for the SW they use on a subscription model, typically monthly or yearly, regardless of how much they use the SW 
* skills 
  * No deep technical skills are required to deploy, use and gain the benefits of SaaS
* Flexibility
  * Users can access the same application data from anywhere

#### Disadvantages 

SW limitations : There can be some limitations to a SW application that might affect how users work. Because you're using as-is SW, you don't have direct control of features. When you're evaluating which SaaS platform is best suited for a workload, be sure to consider any business needs and SW limitations. 


## Cloud Service model comparison 


Iaas   |PaaS  | SaaS
-------|------|--------
The most flexible cloud service| Focus on application development| Pay-as-you-go pricing model
You configure and manage the HW for you app | Platform management is handled by the cloud provider | User pay for the SW they use on a subscription model

## What is serverless computing?

Like Paas, serverless computing enables developer to build application faster by eliminating the need for them to manage infrastructure. With serverless applications, the cloud service provider automatically provisions, scales , and manages the infrastructure required to run the code. Servverless architectures are highly scalable and event-driven, only usign resources when a specific function or trigger occurs.
It's important to note that servers are still running the code. The "serverless" name comes from the fact that the tasks associated with infrastructure provisioning and managemente are invisible to the developer. This approach enables developers to increase their focus on the business logic, and deliver more value to the core of the business. Serverless computing helps teams increase their productivity and bring products to market faster, and it allows organizations to better optimize resources and stay focused on innovation.
