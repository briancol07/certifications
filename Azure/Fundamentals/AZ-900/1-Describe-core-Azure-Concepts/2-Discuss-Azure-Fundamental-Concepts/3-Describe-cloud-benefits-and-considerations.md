# Describe cloud benefits and considerations 

## What are some cloud computing advantages? 

* High Availability :
  * Depending on the service-level agreement (SLA) that you choose, your cloud-based apps can provide a continuous user experience with no apparent downtime, even when things go wrong.
* Scalability : 
  * Apps in the cloud can scale vertically and horizontally 
    * Scale vertically : to increase compute capacity by adding RAM or CPUs to a virtual machine 
    * Scaling Horizontally : Increase compute capacity by adding instances of resources, such as adding VNs to the configurations 
* Elasticity 
  * You can configure cloud-based apps to take advantage of autoscaling, so your apps always have the resource they need 
* Agility 
  * Deploy and configure cloud-based resources quickly as your app requirements change
* Geo-distribution 
  * You can deploy apps and data to regional datacenter around the globe. thereby ensuring that your customers always have the best performance in their region. 
* Disaster recovery 
  * by taking advantage of cloud-based backup services, data replication, and geo-distribution, you can deploy your apps with the confidence that comes from knowing that your data is safe in the event of disaster 

## Capital Expenses vs. Operating expenses 

* Capital Expenditure (CapEx) is the up-front spending of money on physical infrastructure, and then deducting that up-front expenses over time. The up-front cost from CapEx has a value that reducer over time. 
* Operational Expenditure (OpEx) is spending money on services or products now, and being billed for them now. You can deduct this expense in the same year you spend it. There i no up-front cost, as you pay for a service or product as you use it. 
