# Discuss different types ofcloud models 

## What are public, private and hybrid clouds?

### Public Cloud 

Services are offered over the public internet and available to anyone who wants to purchase them. Cloud resources, such as servers and storage are owned and operated by a third-party cloud service provider and delivered over the internet 

* No capital expenditures to scale up
* Applications can be quickly provisioned and deprovisioned 
* Organizations pay only for what they use

### Private Cloud 

A private cloud consists of computing resources used exclusively by users from one business or organization. A private cloud can be physically located at your organization's on-site (on-premises) datacenter or it can be hosted by a third-party service provider 

* HW must be purchased for start-ip and maintenance 
* Organizations have complete control over resources and security 
* Organizations are responsible for HW maintenance and updates 

### Hybrid cloud 

A Hybrid cloud is a computing environment that combines a public cloud and a private cloud by allowing data and applications to be shared between them.

* Provides the most flexibility 
* Organization determine where to run their applications 
* Organizations control secuirty, compliance or legal requirements 



