# Azure regions, availability zones, and region pairs 

Azure is made up of datacenters located around the globe. When you use a service or create a resource such as a SQK database or VM. you're using physical equipment in one or more of these locations. these specific datacenters aren't exposed to users directly. Instead, Azure organizes them into regions.

## Azure regions 

Aregion is a geographical are on the planet that contains at least one but potentially multiple datacenters that are nearby and networked together with a los-latency network. Azure intelligently assigns and controls the resources within each region to ensure workloads are appropiately balanced 

## Why are regions important?

Azure has more global regions than any other cloud provider. These regions give you the flexibility to bring applications closer to your users no matter where they are. Global regions provide better scalability and redundancy. They also preseve dat residency for your services.

## Special Azure regions 

* US DOS Central , US Gov Virginia, and more 
  * These regions are physical and logical network-isolated instances of Azure for U.S. government agencies and partners. These datacenters are operated by screened U.S. personnel and include additional compliance certifications.
* China East, China North and more 
  * These regions are available through a unique partnership between Microsoft and 21Vianet, whereby Microsoft doesn't directly maintain the datacenters.

## Azure availability zones 

You wnat to ensure your services and data are redundant so you can protect your information in case of failure. When you host your infrastructure, setting up your own redundacy requires that you create duplicate HW environments. Azure cna help make your app highly available through availability zones.

## What is an availability zone?

Availability zones are physically separate datacenters within an Azure region. Each availability zone is made up of one or more datacenters equipped with independent power, cooling and networking. An availability zone is set up to be an isolation boundary. If one zone goes down, the other continues working. Availability zones are connected through high-speed, private fiber-optic networks.

## Use availability zones in your apps 

You can use availability zones to run mission-critical applications and build high-availability into your application architecture by co-locating your compute, storage, networking, and data resources within zone and replicating in other zones. Keep in mind that there could be a cost to duplicating your services and transferring data between zones.

* Primarily for:
  * VMS 
  * Managed disks 
  * Load balancers 
  * SQL DB
* Categories 
  * Zonal services  
    * You pin the resource to a specific zone
    * Vms - managed disks - IP addresses 
  * Zone-redundant services
    * The platform replicates automatically across zones 
    * zone-redundant storage - SQK DB
  * Non-regional services
    * Services are always available from Azure geographiss and are resilient to zone-wide outages as well as regin-wide outages.

## Azure region pairs 

Availability zones are created by using one or more datacenters. There's a minimum of three zones within a single region. It's possible that a large disaster could cause an outage big enough to affect even two datacenters. That's why Azure also creates region pairs.

## What is a regin pair?

Each Azure region is always paired with another region within the same geography (such as Us, Europe or Asia) at leat 300 miles away. This approach allows for the replication of resources (such VM storage) across a greography that helps reduce the likelihood of interruptions because of events such as natural disasters, civil unrest, power outages, or physical network outages that affect both regions at once. If a region in a pair was affected by a natural disaster, for instnaace, services would automatically failover to the other region in its region pair .

because the pair of regions is directly connected and far enough apart to be isolated from regional disasters, you can use them to provide reliable services and data redundancy. Some services offer automatic geo0redundant storage by using region pairs.

### Advantages

* If a extensive Azure outage occurs, one region out of every pair is prioritized to make sure at least one is restored as quickly as possible for applicaiton hosted in that region pair 
* Planned Azure updates are rolled out to paired regions one region at a time to minimize downtime and risk of application outage. 
* Data continues to reside within the same geography as its pair (except for Brazil South) for tax- and law-enforcement jurisdiction purposes.
