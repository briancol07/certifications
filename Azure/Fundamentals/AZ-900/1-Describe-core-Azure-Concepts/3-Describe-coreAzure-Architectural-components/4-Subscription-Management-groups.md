# Azure subscription and management groups 

## Azure subscriptions 

Using Azure requires an Azure subscription. A subscription provides you with authenticated and authorized access to Azure products and services. Is a logical unit of Azure services that links to an Azure account, which is an identity in azure Active Directory (Azure AD) or in a directory that Azure AD trusts.

An account can have one or multiple subscriptions that have different billing models and to which you apply different access-management policies.

* Billing boundary 
  * This subscription type determines how an Azure account is billed for using Azure. You can create multiple subscription for different types of billing requirements. Azure generates separate billign reports and invoices for each subscription so that you can organize and manage costs 
* Access control boundary 
  * Azure applies access-management policies at the subscription level, and you can create separate subscriptions to reflect different organizational structures. An example is that within a business, you have different departments to which you apply distinct Azure subscription policies. This billing model allows you to manage and control access to the resources that users provision with specific subscriptions.

## Create additional Azure subscription 

* Environments 
  * When managing your resources you can choose to create subscriptions to set up separate environments for development and testing, security, or to isolate data for compliance reasons. This desing is particularly useful because resource access control occurs at the subscription level.
* Organization structures 
  * You can create subscriptions to reflect different organizational structures. For example you could limit a team to lower-cost resources, while allowing the IT department a full range. This design allows you to manage and control access to the reources that users provision within each subscription. 
* Billing
  * You migh wnat to also create additional subscriptions for billing purposes. Because costs are first aggregated at the subscription level, you might want to create subscriptions to manage ans track costs based on your needs. For instance, you might want to create one subscription for your production workloads and another subscription for your development and testing workloads.
* Subscription limits 
  * Subscriptions are bound to some hard limitations. For example, the maximum number of Azure ExpressRoute circuits per subscription is 10. Those limit should be considered as you create subscriptions on your account. If there's a need to go over those limits in particular scenarios you might need addtional subscriptions 


## Customize biling to meet your needs 

If you have multiple subscriptions, you can organize them into invoice sections. Each invoice sections is a line item on the invoice that shows the charges incurred that month .
Depending on your needs. you can set up multiple invoices within the same biling account. To do this, create additional billing profiles. Each billing profile has its own monthly invoice and payment method.

## Azure management groups 

Azure management groups provide a level of scope above subscriptions. You organize subscriptions into containers called management gorups and apply your governance confitions to the management groups.  All subscription within a management group automatically inherit the conditions applied to the management group . Management gorups give you enterprise-grade management at a large scale no matter what typeof subscriptions you might have. All subscriptions within a single management group must trust the same Azure AD tenant. 

## Hierarchy of management groups and subcriptions 

You can buikd a flexible structure of management groups and subscriptions toorganize your resources into a hierarchy for unified policy and access management. 
You can create a hierarchyt that applies a policy. For example, you could limit a VM location to the US West region in a group called Production .  This policy will inherit onto all the Enterprise Agreement subscriptions that are descendants of that management groups and will apply to all VMs under thsoe subscriptiosn. This security policy can't be altered by the resource or subscription owner, which allows for improved governance. 
Another scenario where you would use management gorups is to provide user access to multiples subscriptiosn . By moving multiple subscriptions under that access to all the subscriptions. One assignment on the management gorup can enable users to ahve access to everything they need instead of scripting RBAC over different subscriptions.

## Important facts about management gorups 

* 10.000 management groups can be supported in a single directory 
* A management group tree can support up to six levels of depth. This limit doesn't include the root level or the subscription level.
* Each management group and subscription can support only one parent
* Each management group can have many children.
* All subscriptions and management groups are withtin a single hierarchy in each directory.
