# Describe core Azure Architectural components 

## Introduction 

### Learning Objectives

* Azure Regions, Region pairs, and availability zones 
* Azure resources, resources groups, Azure resource manager 
* Azure subscriptions and management groups 

## Overview
