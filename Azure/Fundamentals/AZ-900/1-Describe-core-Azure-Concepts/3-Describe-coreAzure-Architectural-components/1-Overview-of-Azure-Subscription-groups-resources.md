# Overview of Azure subscriptions, management groups, and resources 

## Hierarchy of Organizations 

1. Management groups 
  * These groups help you manage access, policy, and compliance for multiple subscriptions. All subscriptions in a management groups: These groups help you manage access, policy, and compliance for multiple subscriptions. All subscriptions in a management group automatically inherit the conditions applied to the management group.
2. Subscriptions 
  * A subscription groups togeteher user accounts and the resources that have been created by those user accounts. For each subscription, there are limits or quotas on the amount of resources that you can create and use. Organizations can use subscriptions to manage costs and the resources that are created by users,teams or proyects. 
3. Resource groups
  * Resources are combined into resources gorups, which act as a logical container into which Azure resources like web apps, databases, and storage acoounts are deployed and managed.
4. Resource 
  * Resources are instances of services that you create, like virtual machines, storage, or SQL db.

