# Azure resources and Azure Resource Manager 

* Resource : 
  * A manageable item that's available through Azure. 
  * Vm - Storage accounts - Web apps - DB - Virtual networks 
* Resource group
  * A container that holds related resources for an Azure solution. The resource group includes resources that you want to manage as a group. You decide which resources belong in a resource group based on what makes the most sense for your organization 

## Azure resource groups 

Resource groups are fundamental element of the Azure platform. A resource group is a logical container for resources deployed on Azure. These resources are anything you create in an Azure subscription like VMs, Azure application gateway instances, and Azure Cosmos Db intances. All resources must be in a resource group, and a resource can only be a member of a single resource group.
Many resources can be moved between resources groups with some services having specific limitations or requirements to move. Resource groups can't ve nested. Before any resource can be provisioned, you need a reource group for it to be placed in. 

## Logical Grouping 

esource groups exist to help manage and organize your Azure resources. By placing resources of similar usage, type, or location in a resource group, you can provide order and organization to resources you create in Azure. Logical grouping is the aspect that you're most interested in here, because there's a lot of disorder among our resources.

## Life cycle 

If you delete a resource group, all resources contained within it are also deleted. Organizing resources by life cycle can be useful in nonproduction environments, where you might try an experiment and then dispose of it. Resource groups make it easy to remove a set of resources all at once 

## Authorization 

Resource groups are also a scope for applying role-based access control (RBAC) permissions. by applying RBAC permissins to a resource group, you can ease administration and limit access to allow only what's needed. 

## Azure Resource manager 

Azure resource manager is the deployment and management service for Azure. It provides a management layer that enables you to create, update, and delete resources in your Azure account. You use management features like access control, locks, and tags to secure and organize your resources after deployment.
When a user sends a request from any of the Azure tools, APIs os SDKs. Resource manager receives the request. it authenticates and authorized the request. Resource Manager sends the request to the azure service, which takes the requested action. Because all requests are handled through the same API, you see cnsistent results and capabilities in all the different tools. 

## The benefits of using Resource Manager 

* Manage your infrastructure through declarative templates rather than scripts. A Resource manager template is a JSON file that defines what you want to deploy to Azure 
* Deploy, manage, and monitor all the resources for your solution as a group, rather than handling these resources individually 
* Redeploy your solution throughout the development life cycle and have confidence your reosurces are deployed in a consistent state
* Define the dependencies between resource so they're deplyd in the correct order 
* Apply access control to all services because RBAC is natively integrated into the management platform.
* Apply tags to resources to logically organize all the resources in your subscription
* Clarify your organization's billing by viewing cost fro a group of resource that share the same tag.


