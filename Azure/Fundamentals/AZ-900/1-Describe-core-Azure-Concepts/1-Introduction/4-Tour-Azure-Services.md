# Azure services 

## Categories 

1. Compute 
2. Networking 
3. Storage 
4. Mobile  
5. Databases 
6. Web
7. IoT (Internet of the things) 
8. Big Data
9. AI (Artificial Intelligence)
10. DevOps

## Compute 

* Azure Virtual Machines 
* Azure Virtual machine scale sets 
* Azure Kubernetes Service 
* Azure Service fabric 
* Azure Batch
* Azure Container instances 
* Azure Functions 

## Networking 

Linking compute reource and providing access to applications is the key function of azure networking. Networking functionality in Azure includes a range of option to connect the outside world to services and features in the global Azure datacenters .

* Azure virtual Network 
* Azure load Balancer
* Azure application Gateway
* Azure VPN Gateway
* Azure DNS 
* Azure content Delivery network
* Azure DDos Protection
* Azure TRaffic Manager 
* Azure ExpressRoute 
* Azure Network Watcher 
* Azure firewall
* Azure Virtual WAN 

## Storage 

* Azure Blob Storage 
* Azure File storage 
* Azure Queue Storage

> These services all share several common characteristics 

* Durable and highly available with redundancy and replication 
* Secure thorugh automatic encryption and role-based access control 
* Scalable with virtually unlimited storage
* Managed, handling maintenance and any critical problems for you 
* Accessible from anywhere in the world over HTTP or HTTPS

## Mobile 

Developers can create mobile back-end services quickly and easily features that used to take time and increase project risks, such as adding corporate sing-in and them connecting to on-premises resources, are now simple to inlcude.

* Other features 
	* Offline data synchronization
	* Connectivity to on-premises data 
	* Broadcasting push notifications 
	* Autoscaling to match business needs 

## Databases 

* Azure Cosmos DB
* Auzre SQL Database
* Azure Database for MySQL 
* Azure Database for PostgreSQL 
* SQL Server on Azure Virtual Machines 
* Azure Database Migration Service 
* Azure Cache for Redis 
* Azure Database for MariaDB

## Web 

Azure includes first-class support to build and host web apps and HTTP-based web services.

* Azure App Service 
* Azure Notification Hub
* Azure API Management
* Azure Cognitive Search 
* Web Apps feature of Azure app Service 
* Azure SignalR Service 

## IoT 

People are able to access more information than ever before. Personal digital assistants led to smartphones, and now there are smart watches, smart thermostats, and even smart refrigerators. Personal computers used to be the norm. Now the internet allows any item that's online-capable to access valuable information. This ability for devices to garner and the realy information for data analysis is referred to as IoT.

* IoT Central 
* Azure IoT Hub
* IoT Edge 

## Big Data 

Data comes in all formats and sizes. When we talk about big data. we're referring to large volumes of data from weather systems, communications systems, gemonic research, imaging platforms, and many other scenarios generate hundreds of gigabyte of data. This amount of data makes it hard to analyze and make decisions. It's often so large that traditional forms of processing and analysis are no longer appropiate. 

Open-Source cluster technologies have been developed to deal with these large data sets. Azure supports a broad range of technologies and services to provide big data and analytic solutions 

* Azure Synapse Analytics 
* Azure HDInsight 
* Azure Databricks 

## AI 

AI, in the context of cloud computing, is based around a broad range of services, the core of which is machine learning . It is a data science technique that allows computers to use existing data to forecast future behaviors outcomes, and tends.Using machine learning, computer learn without being explicity programmed.
Forecasts or predictions from machine learning can make apps and devices smarter. For xample, when you shop online, machine learning helps recommended other products you might like based on what you've purchased. Or when your credit card is swiped, machine learning compares the transaction to a database of transactions and helps detect fraud. And when your robot vacuum cleaner vacuums a room, machine learning helps it decide whether the job is done 

* Azure Machine Learning Service 
* Azure ML Studio 

* API's
  * Vision 
  * Speech 
	* Knowledge mapping 
	* Bing Search
	* Natural Language processing 

## DevOps

DevOps brings together people, processes and technology by automating SW delivery to rpovide continous value to your users. With Azure DevOps, you can create build and release pipelines that provide continuous integration, delivery, and deployment for your applications you cna integrate repositories and application test, perform application monitoring and work with build artifacts. You can also work with and backlog items for tracking,automate infrastructure deployment, and integrate a range of third-party tools and services such as jenkins and Chef. All of these functions and many more are closely integrated with azure to allow for consistent, repeatable deployments for your applications to provide streamlined vuild and release processes 

* Azure DevOps 
* Azure DevTest Labs 
