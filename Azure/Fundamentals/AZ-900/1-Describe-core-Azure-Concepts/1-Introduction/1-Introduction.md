# Introduction 

Azure is a cloud computing platform with an ever-expanding set of services. Range from simple web services for hosting your business presence in the cloud to running fully virtualized computers for you to run your custom SW solutions.

## What is Azure Fundamentals?

Is a series of six learning path that familiarize you to Azure and its many services and features 

* Interested in:
  * Azure's compute
  * Network 
  * Storage 
  * Database 
  * Learning about cloud security best practices 
  * Exploring the cutting edge in IoT 
  * Machine learning 

## Why should I take Azure fundamentals?

* Have general interest in Azure or in the cloud 
* Want to earn official certification from mircrosoft 


Area   |  Weight  
-------|---------
Describe cloud concepts  | 20 - 25 %  
Describe core Azure services | 15 - 20 % 
Describe core solutions and management tools on Azure | 10 - 15 % 
Describe general security and network security features | 10 - 15 %
Describe Identity governance, privacy and compliance features | 20 - 25 % 
Describe Azure cost management and Service Level Agreements | 10 - 15 % 

## Learning objectives 

* Describe basic concepts of cloud computing 
* Determine whether Azure is the right solution for your business needs
* Differentiate between the different methods of creating an Azure subscription

