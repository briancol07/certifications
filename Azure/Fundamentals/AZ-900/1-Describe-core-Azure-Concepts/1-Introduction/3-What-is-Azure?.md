# What is Azure?

Azure is Microsoft's cloud computing platform with an ever expanding set of services to help you to build solutions to meet your business goals.

* Supports
  * Infrastructure as a Service (IaaS) 
  * Platform as a Service (PaaS) 
  * Software as a Servive (SaaS) 

Azure is a continually expanding set of cloud services that help your organization meet your curretn and future business challenges. Azure gives you the freedom to build manage, and deploy applications on a massive global network using your favorite tools and frameworks 

## What does Azure offer? 

With help form Azure, you have everything you need to build your next great solution. The following table list several of the benefits that Azure provides, so you can easily invent with purpose.

* Build on your term 
* Operate Hybrid seamlessly 
* Trust on the cloud 

## What can I do with Azure? 

* AI
* Machine learning
* Storage solutions  

## How does Azure work? 

Virtualization separates thetight coupling between a computer's HW and its OS,using an abstraction layer called a hypervisor. The hypervisor emulates all the functions of a real computer and its CPU in a VM optimizing the capacity of the obstructed HW . It takes this virtualization technology and repeats it on a masive scale. 

Server -> Rack -> Fabric controler -> Orchestrator 

Orchestrator function is to manage everything that happend in Azure 

## What is the Azure portal? 

The azure portal is a web-based,unified console that provides an alternative to command-line tools.

* Build manage, and monitor everything from simple web apps to complex cloud deployments 
* Create custom dashboard for an orgaized view of resources
* Configure accessibility options for an optimal experience 

## What is azure marketplace?

Azure marketplace helps connect users with Microsoft partners, independent SW vendors, and startups that are offering their solution and services, which are optimized to run on Azure.
