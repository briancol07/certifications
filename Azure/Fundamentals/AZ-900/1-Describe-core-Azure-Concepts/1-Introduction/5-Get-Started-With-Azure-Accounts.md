# Get started with Azure accounts 

To create and use Azure services, you need an Azure subscription. When you're working with your own applications and business need, you need to create an Azure Account, and a subscription will be created for you. After you've create an Azure account, you're free to create additional subscriptions.After you've create an Azure subscription, you can start creating Azure resources within each subscription.


```

Azure    --> Subscriptions  --> Resource  --> Resources 
Account                         groups 

``` 

## Crate an Azure account


* Website 
* Microsoft Representative
* Microsoft Partner 

For large companies you can have an : 

Invoice Sections with a billing profile 

## What is the Azure free Account? 

* Includes
  * Free access to popular Azure products for 12 months 
  * A credit to spend for the first 30 days 
  * Acces to more than 25 products that are always free

The Azure free account is an excellent way for new users to get started and explore. 

## What is the Azure free student account? 

* Includes 
  * Free access to certain Azure services for 12 months 
  * A credit to use in the first 12 months 
  * Free access to certain SW developer tools 

## What is the Learn Sandbox?

Many of the learn exercises use a technology called the sandbox, which creates a temporary subscription that's added to your Azure account. This is temporary subscription allows you to create Azure resources for the duration of a learn module. Learn automatically cleans up the temporary resources for you after you've completed the module. 

It allows you to create and test Azure resources at no cost to you. 




