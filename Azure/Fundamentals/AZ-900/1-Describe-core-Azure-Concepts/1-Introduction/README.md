# Introduction to Azure Fundamentals 

## Overview

* [1-Introduction.md](1-Introduction.md)
* [2-What-is-Cloud-computing?.md](2-What-is-Cloud-computing?.md)
* [3-What-is-Azure?.md](3-What-is-Azure?.md)
* [4-Tour-Azure-Services.md](4-Tour-Azure-Services.md)
* [5-Get-Started-With-Azure-Accounts.md](5-Get-Started-With-Azure-Accounts.md)
