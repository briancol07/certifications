# What is cloud computing? 

* Cloud providers 
  * Compute power 
    * Is how much processing your computer can do 
  * Storage 
    * Is the volume of data you can store on your computer ]

## Why is cloud computing typically cheaper to use?

Cloud computing is the delivery of computing services over the internet by using a pay-as-you-go pricing model.

* Lower your operating costs 
* Run your Infrastructure more efficiently 
* Scale as your business need change 

When you're done using them, you give them back. You're billed only for what you use 

## Why should I move to the cloud?

* Teams deliver new features to their users at record speeds 
* Users expect an increasingly rich and immersive experience with their devices and with SW 

* The cloud provides on-demand access to: 
  * Anearly limitless pool of raw compute, sotrage and networking componentes 
  * Speech recognition and other cognitive services that help make your apllication stand out form the crowd 
  * Analytics services that deliver telemtry data from your SW and devices 


