# Azure Virtual Network Fundamentals

## What is Azure virtual Networking ? (AVN)

AVN enable Azure resources, such as VMs, web apps and databases to communicate with each other, with users on the internet and with your on-premises client computers. You can think of an Azure network as an extension of your on-premises network with resources that links other Azure resources. 

* Provide the following capabilities 
  * Isolation and segmentation 
  * Internet communications 
  * Communicate with on-premises resources 
  * Route network traffic 
  * Filter network traffic
  * connect visual networks 

## Isolation and segmentation 

AVN allows you to create multiple isolated virtual networks. When you set up a virtual network, you define a private IP address space by using either public o private IP address ranges . The public IP range only exists within the virtual network and isn't internet routable. You can divide that IP address space into subnets and allocate part of thedefined address space to each named subnet .

## Internet communications 

A Vm in azure can connect to the internet by default. You can enable incomign connections from the internet by assigning a public IP address to the VM or by putting the VM behind a public load balancer. For VM management, you can connect via the Azure CLI, Remote dektop protocol or secure shell.

## Communicate between Azure resources 

* Virtual networks : Virtual networks can connect not only VMs but other Azure resources, such as the app Service Environment for power apps, Azure Kubernetes service and Azure VM scale sets 
* Service endpoints: You can use serviece endpoints to connect to other Azure resources type,such as Azure SQL databases and storage accounts. this approach enables you to link multiple Azure resources to virtual networks to improve security and provide optimal routing between resources .

## Communicate with on-premises resources 

* Point-to-site virtual private networks 
  * The typical approach to a VPN connection is from a computer outside your organization, back into your corporate network, In this case, the client computer initiates an encrypted VPN connection to connect that computer to the AVN
* Site-to-site virtual private networks
  * A site-to-site VPN links your on-premises VPN devide or gateway to the azure VPN gatewat in a virtual network. In effect, the devices in Azure can appear as being on the local network The connection is encrypted and works over the internet.
* Azure Express Route 
  * For Environments where you need greater bandwidth and even highter levels of security, Azure Express route is the best approach. Express Route provides a dedicated private connecitivity to Azure that doesn't travel over the internet. 

## Route Network traffic 

* Route tables
  * A route table allows you to define rules about how traffic should be directed. You can create custom route tables that control how packets are routed between subnets. 
* Border Gateway Protocol 
  * Border gateway protocol (BGP) works with Azure VPN gateways, Azure route server or Express route to propagate on-premises BGP routes to Azure Virtual networks .
 
## Filter network traffic

* Network security groups : A network security group is an Azure resource that can contain multiple inbound and outbound security rules. You can define these rules to allow or block traffic, based on factors such as source and destination IP address, port and protocol
* Network virtual appliances: A network virtual appliance is a specialized VM thtat can be compared to a hardened network appliance. A network virtual appliance carries out a particular network function, such as running a firewall or preforming wide area network(WAN) optimization 

## Connect Virtual Networks

You can link virtual netwroks together by using virtual network peering. Peering enables resources in each virtual network to communicate with each other. These virtual networks can be in separate regions, which allows you to create a global interconnected network through Azure. 

User-defined routes (UDR) are significant update to Azure's Virtual networks that allows for greater control over network traffic flow. This methos allows network administrator to control the routing tables between subnets within a Vnet, as well as between Vnets.
