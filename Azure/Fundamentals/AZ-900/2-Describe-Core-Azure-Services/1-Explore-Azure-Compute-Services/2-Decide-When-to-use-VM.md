# Decide when to use Azure Virtual Machine

* Vms are an ideal choice when you need:
  * Total Control over the Operating system (OS)
  * The ability to run custom SW
  * To use custom hosting configuration

An azure Vm gives you the flexibility of virtualization without having to buy and maintain the physical HW that runs the VM. You still need to configure, update, and maintian the SW that runs.

## Examples of when o use VMs

* During testing and development 
* When running application in the cloud 
* When extending your datacenter to the cloud 
* During disaster recovery 

## Move to the cloud with VMs 

VMs are also an excellent choice when you move from a physical server to the cloud (also known as lift and shift). You can create an image of the physical server and hos it within a VM with little or no changes. Just like a physical on-presmises server, you must maintain th VM.

## Scale VMs in Azure

You can run single VMs for testing, development, or minor taks. Or you can gorup VMs together to provide high availability, scalability and redundancy. No Matter what you uptime requirements are, Azure has several feature that can meet them.

* Virtual machine scale sts 
* Azure Batch 

## What are virtual machine scale sets ?

Virtual machine scale sts let you create and manage a group of identical, load-balance VMs. Imagine you;re runnign a website that enables scientists to upload astronolmy images that need to be processed. If you duplicated the VM, you'd normally need to configure an additional service to route requests between multiple instances of the website. Virtual mahcine scale sets could do that work for you. 
Scale sets allow you to centrally manage, configure, and update a large number of VMs in minutes to provide highly available applications. The number of VM instances can automatically increase or decrease in response to demand or a defined schedule. With VM scale sets, you can guild large-scale services for areas such as compute, big data and container workloads.

## What is Azure Batch ?

Azure batch enables large-scale parallel and high-performance computing (HPC) batch jobs with the ability to scale to tens, hundreds, or thoushand of VMs. 

