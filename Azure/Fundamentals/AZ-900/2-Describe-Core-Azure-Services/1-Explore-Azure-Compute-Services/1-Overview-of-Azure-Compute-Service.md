# Overview of Azure compute service

Azure compute is an on-demand computing service for running cloud-based applications. it porvides computing resources such as disks, processors, memory, networking, and operating systems. The resources are available on-demand and can typically be made available in minutes.You pay only for the resources you use, and only for as long as you're using them.

* Azure VM
* Azure Container Instances 
* Azure App Service
* Azure functions (or serverless computing) 

## Virtual Machines (VM)

Virtual machines are SW emulations of physical computers. They include a virtual processor, memory, storage, and networking resources. VMs host an operating system and you can install and run SW just like a physical computer.
With Azure, you can create and use Vms in the cloud (IaaS).

### VM scale sets 

Are an Azure compute resource that you can use to deploy and manage a set of identical Vms. With all Vms configured the same, VM scale sets are designed to suppor tru autoscale. No pre-provisioning of VMs is required.
For this reason,it's easier to build large-scale services targeting big compute, big data, and containerized workdloads As demand goes up more VM instances can be added , also going down can be removed ( manual or automated or both).

## Container and Kubernetes

Containers are lightweight, virtualized application environments. They're designed to be quickly created, scaled out, and stopped dynamically. You can run multiple instances of a containerized application on a isngle host machine 

## App Service 

You can quickly guild, deploy, and scale enterprise-grade web, mobile and API apps running on any platform. You can meet rigorous performance, scalability, security and compliance requirements while usign a fully managed platform to perfom intrastructure maintenance. App Service is a PaaS.

## Functions

This are ideal when you're concerned only about the code runnign your service and no the underlying platform or infrastructure. They're commonly used when you need to perform work in response to an event (often via a REST request), timer or message from another Azure service, and when that work can be completed quickly within seconds or less.

