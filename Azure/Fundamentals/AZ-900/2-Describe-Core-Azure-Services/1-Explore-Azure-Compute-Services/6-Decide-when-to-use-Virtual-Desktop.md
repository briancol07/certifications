# Decide When to use Azure virtual desktop  (AVD)

## What is Azure virtual Desktop?

Azure virtual Desktop is a desktop and application virtualization service that runs on the cloud . It enables our users to use a cloud'hosted version of windows from any location. Work across devices like windows, mac , ios , android , linux. It works with apps that you can use to access remote desktops and apps.

## Why should you use Azure virtual Desktop ?

Users have the freedom to connect to AVD with any device over the internet. They use a Azure virtual Desktop client to connect to their published window dektop and applications. This client could wither be a native application on the device or the AVD html5 web client .

You can make sure your session host VM run near apps and service that connect to your datacenter or the cloud. This way your users stay productive and don't encounter long load times. 

You can provide individual ownertship through personal(persistent) dekstops. For example, you might want to provide personal remote desktops for memebers of an engineering team. Then they can add or remove programs without impacting other users on that remote desktop.

AVD Provides centralized security management for users' desktops with Azure Active Directory (Azure AD). You can enable multifactor authentication to secure user sign-ins. You can also secure access to data by assigning granular role-based access controls (RBACs) to users 

With AVD, the data and apps are sepeated from the local HW.Runs them instead on a remote server. The risk of confidential data being left on a personal devide is reduced. 

User sessions are isolated in both single and multi-session environments. Azure virtual desktop also improves security by using reverse connect technology. This connection type is more secure than the The remote desktop protocol.

* Provide The best user experience 
* Enhance security 

## What are some Key feature of AVD 

### Simplified management 

AVD is an Azure service, so it will be familiar to Azure administrator. You use Azure Ad and RBACs to manage access to resources. With Azure, you also get tools to automate VM deployment, manage VM updates, and provide disaster recovery. As with other Azure services, Azure virtual Desktop uses Azure Monitor for monitoirng and alerts. This standardization lets admins identigy issues thorugh a single interface. 

### Performance management 

AVD gives you option to load balance users on you VM host pools. Host pools are collections of VMS with the same configuration assigned to multiple users. For the best performance you can configure load balancing to occur as users sign in (breadth mode). With breadth mode users are sequentially allocated across the host pool for your workload. To Save costs, you can confirue your VMs for depth mode load balancing where useres are fully allocated on one VM before moving to hte next. AVD provides tools to automatically provision additional VMs when incoming demand exceeds a specified threshold.

### Multi- Session Windows 1- deployment 

AVD lets you use Windows 10 Enterprise multi-session, The only windows client-based operating system that enables multiple concurrent users on a single VM. Azure virtual Desktop also provides a more consistent experience with broader application support compared to Windows server-based operating systems 

## How can you reduce costs with AVD? 

### Bring your own licenses 

AVD is available to you at no addtional cost if you have an eligible Microsoft 365 lincense, Just pay for the Azure resources used by AVD 

* Bring your eligible Windows or Microsoft 365 license to get windows 10 Enterprise and windows 7 Enterprise desktop and apps at no additional cost 
* If you're an eligible Microsoft Remote Desktop Services Client Access Licence customer , Windows server Remote Desktop Services desktops and apps are available at no additional cost 

### Save on Compute costs 

Buy one-year or three-year Azure Reseved virtual Machine Instances to save you up to 72 percent versus pay-as-you-go pricing. You can pay for a reservation up fron or monthly. reservations provide a billing discoutn and don't affect the runtime state of your resources. 
