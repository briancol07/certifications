# Decide When to use Azure App Service .


App service enables you to build and host web app, backgorund jobs, mobile back-ends and RESTful APis in the programming language of your choice without managing infrastructure (PaaS) 

## Azure App Service Costs 

You pay for the Azure compute resources your app uses while it processes requests based on the App Service plan you choose. The App Service plan determines how much HW is devoted to your host. For example, the plan determines whether it's dedicated or shared HW and how much memory is reserved for it. There's even a free tier you can use to hsot small, low-traffic sites.

## Types of app services 

App Service handles most of the infrastructure decisions you deal with in hosting web-accessible apps:

* Most common services
  * Web apps 
  * API apps 
  * Web jobs
  * Mobila apps 
* Infrastructure decisions
  * Deployment and management are integrated into the platform 
  * Endpoints can be secured
  * Sites can be scaled quickly to handle high traffic loads 
  * The built-in load balancign and traffic manager provide high availability

all of these app styles are hosted in the same infrasturcture and share these benefits. This flexibility makes App Service the ideal choice to host web-oriented applications 

## Web Apps 

App Service includes full support for hosting web apps by usign ASP.NET Core, Java, Ruby, Node.js, PHP, or python. You can choose either Windows or linux as the host OS.


## WebJobs 

you can use the WebJobs feature to run a program or script in the same context as a web app, API app, or mobile app They can be scheduled or run by a trigger. WebJobs are often used to run background tasks as part of your application logic.

## Mobile apps 

Use the mobole Apps feature of App Service to quickly build a back end for IOS and Android apps. With just a few clicks in the azure portal you can: 

* Store mobile app data in a cloud-base SQL database
* Authenticate customers against common social providers, such as MSa, Google, Twitter, and Facebook
* Send push notifications 
* Execute custom back-end logic in C# or Node.js

