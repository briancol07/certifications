# Decide when to use Azure Funcions

Serverless computing is the abstraction of servers, infrastructure, and OS. With serverless computing, Azure takes care of managing the server infrastructure and the allocation and deallocation of resources based on demand .
Infrastructure isn't your responsibility. Scaling and performance are handled automatically. You're billed only for the exaxt resource you use.There's no need to even reserve capacity.

This includes the abstraction of servers, an event-driven scale, and micro-billing 

## Abstraction of servers 

Serverless computing abstracts the server you run on. You never explicitly reserve server instances. The platform manages that for you. Each function execution can run on a different compute instance. 
This execution context is transparent to the code. With serverless architecture you deploy your code, wich the runs with high availability.

## Event-Driven scale 

Serverless computing is an excellent fit for workloads that respond to incoming events:

* Timer
  * If a function needs to run every day at 10 am
* HTTP
  * API and webhook scenarios
* Queues, for example with order processing 

Instrad of writing an entire application, the developer authors a function, which contains both code and metadata about its triggers and bindings. The platform automatically schedules the function to run and scales the number of compute instances based on the rate of incoming events. Triggers define how a function is invoked. Bindings provide a declarative way to connect to services from within the code 

## Micro-billing 

With serverless computing, they pay only for the time their code runs. If no active function executions occur, they're not charged. For example, if the code runs once a day for two minutes, they're charge ofr one execution and two minutes of computing time 

## Azure 2 Implementations of serverless

* Azure Functions 
* Azure Logic apps

### Azure Functions

When you're concerned only about the code running your service, and not the underlying platform or infrastructure, using Azure Functions is ideal. Functions are commonly used whtn you need to perform work in response to an event (often via a rest request), timer or message from another Azure service, and when that work can be completed quickly, within seconds or less.

Funcionts scale automatically based on demand, so they're a solid choice when demand is variable.
Using a VM-based approach, you;d incur costs even when the virtual machine is idle. With functions, Azure runst your code when it's triggered and automatically deallocates resources when the function is finished.

Functions can be either stateless or stateful. When they're stateless (default) they behave as if they're restarted every time they respond to an event. When they're stateful (Durable Functions), a context is passed through the funtion to track prior activity.

Functions are a key component of serverless computing. They're also general compute platform for running any type of code. If the need of the developer's app change, you can deploy the project in an environment that isn't serverless. this flexibility allows you to mange scaling, run on virtual networks, and even cpompletely isolate the funcitons. 
## Azure logic Apps 

Logic apps are similar to funcitons. Both enable you to trigger logic based on an evnet. Where functions execute code, logic apps execute workflows that are designed to automate business and are built from predefined logic blocks.

Every Azure logic app workflow start with a trigger , which fires when a specific even happens or when newly available data meet speciffic criteria. Many triggers include basic scheduling capabilities, so developers can specify how regularly their workloads will run. Each time the trigger fires, the Logic apps engine creates a logic app instance that runs the actions in the workflow. These actiosns can also include data conversions and flow controls, such as conditional statements, switch statements, loops and branching. 

You create lofic app workflows by using a visual designer on the azure portal or in VS. The workflow are persisted as a JSON file with a known workflow schema. 

* Examples 
  * Detect the intent of the message with cognitive services
  * Create an item in SharePoint to track the issue
  * Add the customer to your Dynamics 365 CRM system if they aren't already in your dataase 
  * Send a follow-up email to acknowledge their request

## Functions VS logic Apps 

Functions and Logic apps can both create complex orchestrations. An orchestration is a collection of functions or steps that are executed to accomplish a  complex task.

* With Functions, you write code to complete each step 
* With Logic Apps, you use a GUI to define the actions and how they relate to one and anohter 


    | Function | Logic Apps
----|----------|-------------
State| Normally stateless but durable functions provide state | Stateful
Development | Code-First (imperative) | Designer-first (declarative)
Connectivity | About a dozwn built-in binding ypes. Write code for custom bindings | Large collection of connectors. Enterprise Integration Pack for b2B scenarios . Build custom connectors 
Acctions | Each activity is an Azure Function. Write code for activity functions | Large collection of ready-made actions 
Monitoring | Azure Application Insights | Azure portal , Log Analytics 
Management | Rest API, VS | Azure portal, REST API, powershell, VS
Execution context | Can run locally or in the cloud | Runs ony in the cloud 
