# Decide when to use Azure container instances or Azure Kubernetes Service 

## What are containers? 

Containers are virtualization environment. Much like running multiple VM on a signle physical host, you can run multiple containers on a single physical or virtual host. Unlike VM, you don't manage the OS for a container. VM appear to be an instance of an operating system that you can connect to and manag, but container are lightweight and designed to be created, scaled out, and stopped dynamically. While it's possible to create and deploy VN as application demand increases, containers are designed to allow you to respond to changes on demand. With containers, you cna quickly restart in case of crash or HW interruptio. One of the most popular container engines is Docker, which is supported by Azure. 

## Compare VM to Containers

* VM
  * Virtualize HW
* Container 
  * Virtualize OS
  * More efficient 
  * More than 1 instance per host

## Manage containers 

Container are managed through a container orchestrator, which can start, stop, and scale out application instances as needed. There are two ways to manage both Docker and Microsoft-based containers in Azure: Azure Containe instances and Azure Kubernetes Service (AKS)

### Azure Container Instances 

Offers the fastest and simplest way to run a container in Azure without having to manage any virtual machines or adopt any additional services (PaaS) offering that allows you to upload your containers, Which it runs for you.

### Azure Kubernetes Service

The task of automaitng managing and interacting with a large number of containers is known as orchestration. Is a complete orchestration service for containers with distributed architectures and large volumes of containers.

## What is Kubernetes?

Managing container-based workloads
Combines container management automationwith an extensible API to create a cloud-native application management powerhouse . 
At its core, Kubernetes manages the placement of pods, which can consist of one or more containers, on a Kubernetes clluster node.

## Use containers in your solutions 

Containers are often used to create solution by using a microservice architecture. This architecture is whre you break solution into smaller, independent pieces. For example, you might split a website into a container hosting your fron end, another hosting you back end, and a third for storage. This split allows you to separate portions of your app into logical sections that can be maintained,scaled or updated independently. 

* If your back-end has reached capacity but the fron end and storage aren't beign stressed. You could:
  * Scale the back end separately to improve performance
  * Decide to use a different sotrage service
  * Replace the storage container wihtout affecting the rest of the application 
