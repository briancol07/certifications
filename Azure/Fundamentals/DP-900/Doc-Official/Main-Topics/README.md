# Data Fundamentals DP-900

## Learning Objectives 

* Identify  common data formats 
* Describe options for storing data in files 
* Describe options for storing data in databases 
* Describe characteristics of transactional data processing solutions
* Describe characteristics of analytical data processing solutions 

