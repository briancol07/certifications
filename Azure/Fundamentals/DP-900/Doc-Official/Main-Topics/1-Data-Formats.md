## Identify data formats 

Data is a collection of facts such as numbers, descriptions and observations used  to record information.
Data Structures --> Represent Entities  (customers, products , sales orders, and so on) they have attributes or characteristics 

* Clasification 
  * Structured 
  * Semi-structured
  * Unstructured 

## Structured data 

Adheress to a fixed schema,so all of the data has the same fields or properties (commonly tabular) 
rows = instance 
columns = attributes 

often used in ddbb

## Semi-structured data 

Is information that has some structure, but which allows for some variation between entit instances (commonly json)

## Unstructured 
